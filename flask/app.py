from flask import Flask 

app = Flask(__name__)

@app.route("/")
def index():
    return "First task is Successful! Testing multibranch hook #2."

@app.route("/api/")
def api():
    return "REST Landing spot"

if __name__ == "__main__":
    app.run()

